
from backend.Graph.graph import Graph

class GraphParser:
    def __init__(self, file_name):
        self.graph = Graph(file_name)

    def run(self):
        self.graph.fill_nodes_data()
        self.graph.fill_edges_data()
        self.graph.link_end_with_start_node()
        try:
            self.graph.remove_decision_nodes()
        except:
            pass

    def print_nodes(self):
        print("Custom nodes")
        for node in self.graph.nodes:
            print(node.id, node.type, node.label, sep=', ')

        print("Edges")
        for edge in self.graph.graph_from_file.edges(data=True):
            print(edge)

        print("Custom edges")
        for edge in self.graph.edges_list:
            print(edge.id, edge.from_node_id, edge.to_node_id, sep=', ')
