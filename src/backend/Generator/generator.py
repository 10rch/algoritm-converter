
from xml.dom import minidom

from backend.Graph.graph import Graph
from backend.Graph.node import Node, NodeType

x_global = 1
y_global = 1

class Generator:
	def __init__(self, graph: Graph):
		self.root = minidom.Document()
		self.graph = graph


	def create_lts(self):
		self.lts = self.root.createElement('LTS')
		self.root.appendChild(self.lts)

		self.create_declaration()

		self.create_processes()
		self.create_states()

		for node in self.graph.nodes:
			self.add_state(node)

		self.create_links()

		lts_str = self.root.toprettyxml(indent="  ")

		return lts_str

	def _save_lts_to_file(self, file_path: str, data_string: str):
		with open(file_path, "w") as f:
			f.write(data_string)

	def create_declaration(self):
		declaration = self.root.createElement('Declaration')
		self.lts.appendChild(declaration)

	def create_processes(self):
		processes = self.root.createElement('Processes')
		self.lts.appendChild(processes)

		self.process = self.root.createElement('Process')
		self.process.setAttribute('Name', 'New_Process')
		self.process.setAttribute('Parameter', '')
		self.process.setAttribute('Zoom', '1')
		self.process.setAttribute('StateCounter', str(len(self.graph.nodes)))

		processes.appendChild(self.process)

	def create_states(self):
		self.states = self.root.createElement('States')
		self.process.appendChild(self.states)

	def add_state(self, node: Node):
		global x_global
		global y_global

		x = x_global
		y = y_global
		width = 0.2

		x_label_offset = 0.1
		y_label_offset = 0.3

		state = self.root.createElement('State')
		state.setAttribute('Name', node.label)

		if(node.type == NodeType.start1 or node.type == NodeType.start2):
			state.setAttribute('Init', 'True')
		else:
			state.setAttribute('Init', 'False')

		self.set_position(state, x, y, width)

		label = self.root.createElement('Label')
		self.set_position(label, x - x_label_offset, y + y_label_offset, width)

		state.appendChild(label)

		self.states.appendChild(state)

		x_global = x_global + 1


	def set_position(self, parent, x: float, y: float, width: float):
		position = self.root.createElement('Position')
		position.setAttribute('X', str(x))
		position.setAttribute('Y', str(y))
		position.setAttribute('Width', str(width))

		parent.appendChild(position)

	def create_links(self):
		self.links = self.root.createElement('Links')
		self.process.appendChild(self.links)

		for edge in self.graph.edges_list:
			node_from = self.graph.find_node_by_id(edge.from_node_id)
			node_to = self.graph.find_node_by_id(edge.to_node_id)
			self.add_link(node_from.label, node_to.label)


	def add_link(self, from_state: str, to_state: str):
		link = self.root.createElement('Link')

		from_link = self.root.createElement('From')
		from_link.appendChild(self.root.createTextNode(from_state))

		to_link = self.root.createElement('To')
		to_link.appendChild(self.root.createTextNode(to_state))

		link.appendChild(from_link)
		link.appendChild(to_link)

		link.appendChild(self.root.createElement('Select'))
		link.appendChild(self.root.createElement('Event'))
		link.appendChild(self.root.createElement('ClockGuard'))
		link.appendChild(self.root.createElement('Guard'))
		link.appendChild(self.root.createElement('Program'))
		link.appendChild(self.root.createElement('ClockReset'))

		label = self.root.createElement('Label')
		self.set_position(label, 1, 0, 0.2)



		self.links.appendChild(link)


