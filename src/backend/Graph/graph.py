
import networkx as nx

from backend.Graph.node import Node, NodeType
from backend.Graph.edge import Edge

class Graph:
    def __init__(self, file_name: str):
        self.graph_from_file = nx.read_graphml(file_name)
        self.nodes: list[Node] = list()
        self.edges_list: list[Edge] = list()

    def fill_nodes_data(self, data=True):
        for node_data in self.graph_from_file.nodes(data=data):
            new_node = Node()
            new_node.fill_node_data(node_data)
            self.nodes.append(new_node)

    def fill_edges_data(self, data=True):
        for edge in self.graph_from_file.edges(data=data):
            new_edge = Edge()
            new_edge.fill_edge_data(edge)
            self.edges_list.append(new_edge)

    def link_end_with_start_node(self):
        try:
            start_node = self.find_node_by_type(NodeType.start1)
            end_node = self.find_node_by_type(NodeType.terminator)
            self._relink_nodes(end_node, start_node)
        except:
            print("No terminal node")

    def _relink_nodes(self, from_node: Node, to_node: Node):
        for edge in self.edges_list:
            if edge.to_node_id == from_node.id:
                edge.to_node_id = to_node.id

        self.nodes.remove(from_node)

    def remove_decision_nodes(self):
        found_decision_nodes = self.find_all_nodes_by_type(NodeType.decision)
        for decision_node in found_decision_nodes:
            self._remove_decision_node(decision_node)

    def _remove_decision_node(self, decision_node: Node):
        decision_node = self.find_node_by_type(NodeType.decision)

        edges_found = self._find_edges_by_node(decision_node)
        self._create_new_edges_without_decision_node(edges_found)

        self.nodes.remove(decision_node)


    def _create_new_edges_without_decision_node(self, edges_found: dict):
        for to_decision_edge in edges_found['to']:
            for from_decision_edge in edges_found['from']:
                new_edge = Edge()
                new_edge.create_edge(to_decision_edge.from_node_id, from_decision_edge.to_node_id)
                self.edges_list.append(new_edge)



    def _find_edges_by_node(self, node: Node):
        # print('Node id', node.id)
        from_edges_found = list()
        to_edges_found = list()
        edges_found = dict()
        for edge in self.edges_list:
            if edge.from_node_id == node.id:
                # print('from_edges_found', edge.id)
                from_edges_found.append(edge)
            if edge.to_node_id == node.id:
                # print('to_edges_found', edge.id)
                to_edges_found.append(edge)

        edges_found['from'] = from_edges_found
        edges_found['to'] = to_edges_found

        for edge in from_edges_found + to_edges_found:
            self.edges_list.remove(edge)

        return edges_found

    @property
    def edges(self):
        return self.graph_from_file.edges(data=True)

    def find_node_by_id(self, node_id: str):
        return next(node for node in self.nodes if node.id == node_id)

    def find_node_by_type(self, node_type: NodeType):
        return next(node for node in self.nodes if node.type == node_type)

    def find_all_nodes_by_type(self, node_type: NodeType):
        result = [node for node in self.nodes if node.type == node_type]
        return result
