
from enum import Enum
# from transliterate import translit

symbols = (u"абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ",
           u"abvgdeejzijklmnoprstufhccss_y_euaABVGDEEJZIJKLMNOPRSTUFHZCSS_Y_EUA")

tr = {ord(a):ord(b) for a, b in zip(*symbols)}

class NodeType(Enum):
    undefined = 0
    start1 = 1
    start2 = 1
    process = 2
    terminator = 3
    decision = 4


class Node:
    def __init__(self):
        self.id = None
        self.type = NodeType.undefined
        self.label = str()

    def fill_node_data(self, node_data):

        node_type: str = node_data[1]['shape_type']
        node_type = node_type.replace('com.yworks.flowchart.', '')

        node_label: str = node_data[1]['label']
        node_label = node_label.translate(tr)
        # node_label = translit(node_label.lower(), 'ru', reversed=True)
        node_label = (node_label.lower()).replace(" ", "_")
        # print(node_label)

        self.id = node_data[0]
        self.type = NodeType[node_type.lower()]
        self.label = node_label
        