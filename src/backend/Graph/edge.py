
edge_count = 0

class Edge:
	def __init__(self):
		global edge_count
		self.from_node_id = str()
		self.to_node_id = str()
		self.id = str()
		edge_count += 1

	def fill_edge_data(self, edge):
		self.from_node_id = edge[0]
		self.to_node_id = edge[1]

		try:
			self.id = edge[2]['id']
		except:
			self.id = 'e' + str(edge_count)

	def create_edge(self, from_node_id, to_node_id, edge_id=''):
		self.from_node_id = from_node_id
		self.to_node_id = to_node_id
		if edge_id == '':
			self.id = 'e' + str(edge_count)
		else:
			self.id = edge_id
