from PyQt5.QtWidgets import (QFileDialog,QLabel,
							 QPushButton, QVBoxLayout,
							 QWidget, QHBoxLayout)

from PyQt5.QtCore import Qt, QFileInfo
from PyQt5.QtGui import QIcon, QFont

from backend.Parser.parser import GraphParser
from backend.Generator.generator import Generator

import os, sys


class MainWindow(QWidget):
	def __init__(self):
		super().__init__()
		self.opened_file_name = ""
		self.initializeUI()

	def initializeUI(self):
		"""Set up the application's GUI."""
		self.setFixedSize(500, 300)
		self.setWindowTitle("Конвертер алгоритмов")

		try:
			base_path = sys._MEIPASS
		except:
			base_path = os.path.abspath(".")

		icon_file_path = os.path.join(base_path, "res/icon.png")
		self.setWindowIcon(QIcon(icon_file_path))
		self.setUpMainWindow()
		# self.createActions()
		# self.createMenu()
		self.show()

	def setUpMainWindow(self):

		"""Create and arrange widgets in the main window."""
		header_label = QLabel("Загрузите файл в формате GraphML")
		header_label.setFont(QFont("Arial", 14))
		header_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
		header_label.setWordWrap(True)

		open_file_btn = QPushButton("Загрузить файл")
		open_file_btn.setFixedSize(200, 100)
		open_file_btn.setFont(QFont("Arial", 14))
		open_file_btn.setStyleSheet("QPushButton { text-align: center; }")
		open_file_btn.pressed.connect(self.open_file)

		self.convert_file_btn = QPushButton("Конвертировать")
		self.convert_file_btn.setFixedSize(200, 100)
		self.convert_file_btn.setFont(QFont("Arial", 14))
		self.convert_file_btn.setStyleSheet("QPushButton { text-align: center; }")
		self.convert_file_btn.pressed.connect(self.convert)
		self.convert_file_btn.setEnabled(False)

		self.log_label = QLabel("")
		self.log_label.setFont(QFont("Arial", 12))
		self.log_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
		self.log_label.setWordWrap(True)


		info_label = QLabel("Программа предназначена для конвертации алгоритма в формате graphML программы yEd в граф в формате lts для использования в программе PAT3. ")
		info_label.setFont(QFont("Arial", 8))
		info_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
		info_label.setWordWrap(True)

		main_v_box = QVBoxLayout()
		main_v_box.addWidget(header_label)
		main_v_box.setSpacing(0)

		buttons_box = QHBoxLayout()
		buttons_box.addWidget(open_file_btn, alignment=Qt.AlignHCenter)
		buttons_box.addWidget(self.convert_file_btn, alignment=Qt.AlignHCenter)

		main_v_box.addLayout(buttons_box)
		main_v_box.addWidget(self.log_label)
		main_v_box.addWidget(info_label)

		self.setLayout(main_v_box)

	def open_file(self):
		file_name, _ = QFileDialog.getOpenFileName(
			self, "Открыть файл", "",
			"Файл GraphML (*.graphML)")

		if(file_name != ""):
			self.opened_file_name = file_name
			self.convert_file_btn.setEnabled(True)
			self.set_log_message(f"Загружен файл: {QFileInfo(self.opened_file_name).fileName()}")

	def save_to_file(self, text_to_save: str):
		file_name, _ = QFileDialog.getSaveFileName(
			self, "Сохранить файл", "",
				  "Labeled Transition Systems (*.lts)")

		try:
			with open(file_name, "w") as f:
				f.write(text_to_save)
		except:
			pass

	def convert(self):
		try:
			p = GraphParser(self.opened_file_name)
			p.run()
			# p.print_nodes()

			gen = Generator(p.graph)
			text_to_save = gen.create_lts()

			self.set_log_message("Конвертация прошла успешно")
			self.save_to_file(text_to_save)
		except:
			self.set_log_message("Ошибка конвертации.\n Файл должен содержать только блоки начала, конца, процесса и решения")



	def set_log_message(self, msg: str):
		self.log_label.setText(msg)