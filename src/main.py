import sys

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication

from backend.Parser.parser import GraphParser
from backend.Generator.generator import Generator
from frontend.main_window import MainWindow

if __name__ == '__main__':
    # p = GraphParser("./res/example/testGraph.graphml")
    # p.run()
    # p.print_nodes()
    #
    # gen = Generator(p.graph)
    # gen.create_lts()
    app = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec())
