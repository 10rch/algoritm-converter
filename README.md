
### Конвертер алгоритмов
___
#### Сборка

Для сборки программы требуется выполнить следующие команды:
```bash
cd build
mkdir build
pyinstaller --noconfirm --onefile --windowed --icon "../res/icon.png" --name "Конвертер алгоритмов" --add-data "../res/icon.png;./res"  "../src/main.py"
```
___
#### Дополнительная информация
- Программа принимает на вход файл в формате .graphml, созданный в программе yEd (десктопная версия).
Программу yEd можно бесплатно скачать по ссылке: [Скачать yEd](https://www.yworks.com/products/yed/download)

- Для успешной конвертации алгоритма, алгоритм должен содержать хотя бы один из следующих типов блоков: start1, start2, terminator, process, decision. При наличии хотя бы одного блока другого типа программа сообщит об ошибке конвертации.

- Программа конвертирует алгоритм в граф в формате .lts, который используется программой PAT3.
Программу PAT3 можно бесплатно скачать по ссылке [Скачать PAT3](https://pat.comp.nus.edu.sg/?page_id=2587) после регистрации

- Расположение элементов графа на рабочем пространстве программы PAT3 не выполняется автоматически и должно производитсья вручную


### Algorithm Converter
___
#### Build

To successfully build  use commands down below:
```bash
cd build
mkdir build
pyinstaller --noconfirm --onefile --windowed --icon "../res/icon.png" --name "Algoritm Converter" --add-data "../res/icon.png;./res"  "../src/main.py"
```
___
#### Additional info
- Program takes .graphml file, made in yEd (desktop version), as input.
yEd can be downloaded for free via link: [Download yEd](https://www.yworks.com/products/yed/download)

- To successfully algorithm conversion algorithm must contain one of listed types of blocks: start1, start2, terminator, process, decision. If algorithm contains at least one block not listed here converter will return an error of conversion.

- Program converts algorithm to graph in format .lts, which can be opened by PAT3.
PAT3 can be downloaded for free via link [Download PAT3](https://pat.comp.nus.edu.sg/?page_id=2587) after registration

- Graph nodes and edges placement at workspace of PAT3 program is not done automatically and should be done manually